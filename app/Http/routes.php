<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\Student;

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/home', function(){
    $Students = Student::all();
    return View ('home')->with('datas', $Students);
});

//get students
Route::get('/', function(){
    $Students = Student::all();
    return View ('welcome')->with('datas', $Students);
});

Route::get('insert', function()
{
    return View ('students/insert');
});
Route::post('students/insert', 'StudentController@insert');

Route::resource('students', 'StudentController');

Route::get('students/delete/{id}', array('as' => 'delete', 'uses' => 'StudentController@delete'));

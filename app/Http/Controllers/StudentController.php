<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Student;

class StudentController extends Controller
{
    public function insert(Request $request){

        $student = Student::create($request->all());
//        $students->nim       = Input::get('nim');;
//        $students->nama      = Input::get('nama');;
//        $students->jeniskelamin = Input::get('jeniskelamin');;
//        $students->tempatlahir       = Input::get('tempatlahir');;
//        $students->umur      = Input::get('umur');;
//        $students->alamat = Input::get('alamat');;
        $student->save();
        return Redirect::route('Home')->withPesan('Data has been saved.');
    }

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $student = Student::create($request->all());

        $student->save();

        return redirect()->action('HomeController@index');
    }


    public function show($id)
    {
        $student = Student::find($id);
        return view('students.show', array('student' => $student));
    }

    public function edit($id)
    {
        $jeniskelamin = array(
            'Laki-laki' => 'Laki-laki',
            'Perempuan' => 'Perempuan');

        $student = Student::find($id);
        return View('students.edit', array('jeniskelamin' => $jeniskelamin, 'student' => $student));
    }

    public function update(Request $request, $id)
    {

            $ganti = Student::find($id);

            $ganti->nama			= $request->input('nama');
            $ganti->jeniskelamin 			= $request->input('jeniskelamin');
            $ganti->tanggallahir 	= $request->input('tanggallahir');
            $ganti->umur 		= $request->input('umur');
            $ganti->alamat 			= $request->input('alamat');
            $ganti->save();
            return redirect()->route('students.show', [$id]);

    }


    public function destroy($id)
    {
        Student::find($id)->delete();
        return Redirect::back()->withMessage('Data has been delete.');
    }

    public function delete($id)
    {
        Student::find($id)->delete();
        return redirect()->action('HomeController@index');
    }

}

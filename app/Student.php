<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent;
class Student extends Model
{
    protected $fillable = [
        'nim', 'nama', 'jeniskelamin', 'tanggallahir', 'umur', 'alamat',
    ];
}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    Student Information
                    <table class="table">
                        <tr>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Tanggal Lahir</th>
                            <th>Umur</th>
                            <th>Alamat</th>
                        </tr>
                        @foreach($datas as $Students)
                            <tr>
                                <td>{!! $Students->nim !!} </td>
                                <td>{!! $Students->nama !!}  </td>
                                <td>{!! $Students->jeniskelamin !!}  </td>
                                <td>{!! $Students->tanggallahir !!}  </td>
                                <td>{!! $Students->umur !!}  </td>
                                <td>{!! $Students->alamat !!}  </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">View {!! $student->nama !!}</div>

                    <div class="panel-body">

                        <table class="table">
                            <tr>
                                <th>NIM</th>
                                <th>Nama</th>
                                <th>Jenis Kelamin</th>
                                <th>Tanggal Lahir</th>
                                <th>Umur</th>
                                <th>Alamat</th>
                            </tr>
                            {{--@foreach($data as $Students)--}}
                                <tr>
                                    <td>{!! $student->nim !!} </td>
                                    <td>{!! $student->nama !!}  </td>
                                    <td>{!! $student->jeniskelamin !!}  </td>
                                    <td>{!! $student->tanggallahir !!}  </td>
                                    <td>{!! $student->umur !!}  </td>
                                    <td>{!! $student->alamat !!}  </td>
                                </tr>
                            {{--<a href="{{ route('/home') }}">Kembali ke Index</a>--}}
                            {{--@endforeach--}}
                        </table>
                        <li><a href="{{ url('/home') }}">Back</a></li>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

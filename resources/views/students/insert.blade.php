@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <h3>Insert Students</h3>

                        {!! Form::open(array('action' => 'StudentController@store')) !!}

                        {!! Form::label('nim', 'NIM') !!}

                        {!! Form::text('nim', '', array('class' => 'form-control')) !!}

                        {!! Form::label('nama', 'Name')  !!}

                        {!! Form::text('nama', '', array('class' => 'form-control')) !!}

                        {!! Form::label('jeniskelamin', 'Jenis Kelamin')  !!}

                        {!! Form::text('jeniskelamin', '', array('class' => 'form-control')) !!}

                        {!! Form::label('tanggallahir', 'Tanggal Lahir')  !!}

                        {!! Form::text('tanggallahir', '', array('class' => 'form-control')) !!}

                        {!! Form::label('umur', 'Umur')  !!}

                        {!! Form::text('umur', '', array('class' => 'form-control')) !!}

                        {!! Form::label('alamat', 'Alamat')  !!}

                        {!! Form::text('alamat', '', array('class' => 'form-control')) !!}

                        {!! Form::submit('Submit', array('class' => 'btn btn-primary'))  !!}

                        {!! Form::close()  !!}


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




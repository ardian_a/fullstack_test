@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Update  {!! $student->nama !!}</div>

                    {!! Form::model($student, array('route' => array('students.update', $student->id), 'method' => 'PUT')) !!}
                    {!! Form::label('nama', 'Nama') !!}

                    {!! Form::text('nama', $student->nama) !!}

                    @if($errors->has('nama'))
                    {{ $errors->first('nama') }}
                    @endif
                    <br/>
                    {{ Form::label('tanggallahir', 'Tanggal Lahir') }}
                    {{ Form::text('tanggallahir', $student->tanggallahir) }}
                    <!-- Penjelasan sama seperti diatas -->
                    @if($errors->has('tanggallahir'))
                    {{ $errors->first('tanggallahir') }}
                    @endif
                    <br/>
                    {{ Form::label('jeniskelamin', 'Jenis Kelamin') }}
                    <!-- Untuk select, paameter 1 = id, parameter 2 = option, parameter 3 = value -->
                    {{ Form::select('jeniskelamin', $jeniskelamin, $student->jeniskelamin) }}
                    @if($errors->has('jeniskelamin'))
                    {{ $errors->first('jeniskelamin') }}
                    @endif
                    <br/>
                    {{ Form::label('umur', 'Umur') }}
                    {{ Form::text('umur', $student->umur) }}
                    @if($errors->has('umur'))
                    {{ $errors->first('umur') }}
                    @endif
                    <br/>
                    {{ Form::label('alamat', 'Alamat') }}
                    {{ Form::text('alamat', $student->alamat) }}
                    @if($errors->has('alamat'))
                    {{ $errors->first('alamat') }}
                    @endif
                    <br/>
                    {!! Form::submit('edit') !!}
                    {!! Form::close() !!}
                    {{--<a href="{!! route('../home') !!}">Kembali ke index</a>--}}
                </div>
            </div>
        </div>
    </div>
@endsection

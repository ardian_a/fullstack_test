@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                @endif


                <div class="panel-body">
                    Welcome {{ Auth::user()->name }},
                    <table class="table">
                        <tr>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Tanggal Lahir</th>
                            <th>Umur</th>
                            <th>Alamat</th>
                        </tr>
                        @foreach($datas as $Students)
                            <tr>
                                <td>{!! $Students->nim !!} </td>
                                <td>{!! $Students->nama !!}  </td>
                                <td>{!! $Students->jeniskelamin !!}  </td>
                                <td>{!! $Students->tanggallahir !!}  </td>
                                <td>{!! $Students->umur !!}  </td>
                                <td>{!! $Students->alamat !!}  </td>

                                <td>
                                    <a href="{{ route('students.show', $Students->id) }}">Show</a>
                                    <a href="{{ route('students.edit', $Students->id) }}">Edit</a>
                                    <a href="{{ route('delete', $Students->id) }}">Hapus</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <li><a href="{{ url('/insert') }}">Insert</a></li>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
